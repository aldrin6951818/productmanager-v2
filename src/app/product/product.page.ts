import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Product } from '../models/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  product: Product = {
    presupuesto: 0,
    unidad: '',
    producto: '',
    cantidad: 0,
    valor_unitario: 0,
    valor_total: 0,
    fecha_adquisicion: '',
    proveedor: ''
  };
  isEditing = false;

  constructor(private route: ActivatedRoute, private router: Router, private productService: ProductService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async params => {
      const id = params.get('id');
      if (id) {
        this.isEditing = true;
        this.product = await this.productService.getProductById(+id);
      }
    });
  }

  calculateTotal() {
    this.product.valor_total = this.product.cantidad * this.product.valor_unitario;
  }

  async saveProduct() {
    if (this.product.valor_total > this.product.presupuesto) {
      alert('El valor total no puede exceder el presupuesto.');
      return;
    }

    try {
      if (this.isEditing) {
        await this.productService.updateProduct(this.product.id!, this.product);
        alert('Producto actualizado correctamente.');
      } else {
        await this.productService.createProduct(this.product);
        alert('Producto creado correctamente.');
      }
      this.router.navigate(['/']);
    } catch (error) {
      console.error('Error al guardar el producto', error);
      alert('Hubo un error al guardar el producto. Por favor, inténtalo de nuevo.');
    }
  }
}
