import { Injectable } from '@angular/core';
import axios from 'axios';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  baseUrl: string = 'http://localhost:3000/products';

  async getAllProducts(): Promise<Product[]> {
    const response = await axios.get(this.baseUrl);
    return response.data;
  }

  async getProductById(id: number): Promise<Product> {
    const response = await axios.get(`${this.baseUrl}/${id}`);
    return response.data;
  }

  async createProduct(product: Product) {
    const response = await axios.post(this.baseUrl, product);
    return response.data;
  }

  async updateProduct(id: number, product: Product) {
    const response = await axios.put(`${this.baseUrl}/${id}`, product);
    return response.data;
  }

  async deleteProduct(id: number) {
    const response = await axios.delete(`${this.baseUrl}/${id}`);
    return response.data;
  }
}
